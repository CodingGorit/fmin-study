# fmin-study

## 新建仓库规则

1. 以 main 分支为基线
2. 拉取新分支 `git checkout -b ts-study`
3. 然后再新分支上开发

## git log template

```bash
eg 1:

Author: CodingGorit<gorit@qq.com>.

Commit Date: 2022/9/18.
Commit Type: NEW BRANCH.
Commit Comment: add ts-study branch and add ts study.md.

eg 2:
feat: new features devlopment
```

## update log  

- merge tution branch - 2023年7月31日
- 更新 git commit message template —— 2023年2月21日
- express-ts 新增作业接口，@小朱 和 @小石 提交 —— 2022年10月9日
- 新增 fmin-tuition 分支，2022年9月24日 授课内容 —— 2022年9月25日
- 完善 express-ts cli 架构，增加 nacos，mysql 依赖，增加 rest 接口 2022年9月22日
- 拉取 template-study 分支，拉取 express-study 分支。添加 express 基础 demo，2022年9月19日
- 拉取 ts-study 分支，2022年9月18日
