import { Express, Request, Response, Router } from 'express';
import user from './api/private/v1/user';
import work from './api/private/v1/work';
import common from './api/private/v1/common';
import filters from './api/private/v1/filter';
import { Result } from '../common/Result';
import { ResultCodeEnum, ResultMessageEnum } from '../enums/ResultEmums';
const prefix = "/api/v1/tools";

// 路由配置接口
export interface IRouterConf {
  path: string,
  router: Router,
  meta?: unknown
}

// 路由配置
const routerConf: Array<IRouterConf> = [];
routerConf.push(...user, ...work);

// 公共路由配置
const commonRouterConf: Array<IRouterConf> = [...common];
const filterRouteConf: Array<IRouterConf> = [...filters];

function routes(app: Express) {
  filterRouteConf.forEach((conf: IRouterConf) => app.use(conf.path, conf.router));

  // 根目录
  app.get( prefix, (req: Request, res: Response) => res.status(200).send('Hello Shinp!!!'));

  app.get(prefix + "/get", function(req, res) {
    return res.json(new Result(ResultCodeEnum.SUCCESS, ResultMessageEnum.SUCCESS));
  })

  routerConf.forEach((conf) => app.use(prefix + conf.path, conf.router));

  commonRouterConf.forEach((conf: IRouterConf) => app.use(conf.path, conf.router));
}

export default routes;