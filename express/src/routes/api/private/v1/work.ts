/**
 * Created Date: Sunday, October 9th 2022, 11:33:18 pm
 * Author: CodingGorit
 * -----
 * Last Modified: Sun Oct 16 2022
 * Modified By: CodingGorit
 * -----
 * Copyright (c) 2022 fmin-courses
 * ------------------------------------
 * Javascript will save your soul!
 */


// 所有的作业 api 接口
const express = require('express');
import { IRouterConf } from "../../..";
import { Request, Response, NextFunction } from 'express';
import { Result } from "../../../../common/Result";
import { ResultCodeEnum, ResultMessageEnum } from "../../../../enums/ResultEmums";
import log from "../../../../utils/log";

const work = express.Router();

// http://localhost:1347/api/v1/tools/work/query?aaa=3
work.get("/query",
    function (req: Request, res: Response, next: NextFunction) {
        // 参数验证
        // if (!req.query.pagenum || req.query.pagenum <= 0) return res.send(null, 400, "pagenum 参数错误");
        // if (!req.query.pagesize || req.query.pagesize <= 0) return res.sendResult(null, 400, "pagesize 参数错误");
        // log.info(`/work/query ${JSON.stringify(req.query)}`);
        next();
    },

    function (req: Request, res: Response, next: NextFunction) {
        res.json(new Result<string>(ResultCodeEnum.QUERY_SUCCESS, ResultMessageEnum.QUERY_SUCCESS, req.query).toString());
    }
);

// http://localhost:1347/api/v1/tools/work/path/33
work.get("/path/:id",
    function (req: Request, res: Response, next: NextFunction) {
        // 参数验证
        // if (!req.query.pagenum || req.query.pagenum <= 0) return res.send(null, 400, "pagenum 参数错误");
        // if (!req.query.pagesize || req.query.pagesize <= 0) return res.sendResult(null, 400, "pagesize 参数错误");
        // log.info(`/work/query ${JSON.stringify(req.query)}`);
        next();
    },

    function (req: Request, res: Response, next: NextFunction) {
        res.json(new Result<string>(ResultCodeEnum.QUERY_SUCCESS, ResultMessageEnum.QUERY_SUCCESS, req.params.id).toString());
    }
)

// @see /test/index.html workTest  post JSON 格式
work.post("/body/data",
    function (req: Request, res: Response, next: NextFunction) {
        // 参数验证
        next();
    },

    function (req: Request, res: Response, next: NextFunction) {
        res.json(new Result<string>(ResultCodeEnum.QUERY_SUCCESS, ResultMessageEnum.QUERY_SUCCESS, req.body).toString());
    }
);

// @see /test/index.html workFormTest post form 表单格式
work.post("/form/data",

    function (req: Request, res: Response, next: NextFunction) {
        // 参数验证
        next();
    },

    function (req: Request, res: Response, next: NextFunction) {
        res.json(new Result<string>(ResultCodeEnum.QUERY_SUCCESS, ResultMessageEnum.QUERY_SUCCESS, req.body).toString());
    }

)

const routes: Array<IRouterConf> = [{
    path: "/work",
    router: work
}];

export = routes;
