/**
 * Created Date: Sunday, October 16th 2022, 10:07:43 pm
 * Author: CodingGorit
 * -----
 * Last Modified: Sun Oct 16 2022
 * Modified By: CodingGorit
 * -----
 * Copyright (c) 2022 fmin-courses
 * ------------------------------------
 * Javascript will save your soul!
 */
const express = require('express');
import { IRouterConf } from "../../..";
import { Request, Response, NextFunction } from 'express';
import log from "../../../../utils/log";
import { Result } from "../../../../common/Result";
import { ResultCodeEnum, ResultMessageEnum } from "../../../../enums/ResultEmums";
import Utils from "../../../../utils/Utils";

const utils = Utils.getInstance();
const ignorePath = utils.getConfig<Array<string>>("ignorePath");

const filters = express.Router();
const TAG = "filters";

filters.all("*",
    function (req: Request, res: Response, next: NextFunction) {
        log.info(`${TAG} url => ${req.originalUrl}`);
        log.info(`${TAG} method => ${req.method}`);
        // req.params 需要在对的 path 上才能拿得到
        log.info(`${TAG} query is => ${JSON.stringify(req.query)}, data is => ${JSON.stringify(req.body)}`);

        if (ignorePath.includes(req.originalUrl)) {
            log.debug(`${TAG} not support path`);
            return;
        }

        next();
    }
);

const routes: Array<IRouterConf> = [{
    path: "/**",
    router: filters
}];

export = routes;


