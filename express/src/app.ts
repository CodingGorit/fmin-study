// src/app.ts
import express from 'express';
import bootstrap from './middleware';

const app = express();

// 挂载中间件
bootstrap(app);

