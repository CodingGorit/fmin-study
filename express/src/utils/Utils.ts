/**
 * @Author Gorit
 * @Date 2022年9月19日
 */

import config from 'config';

class Utils {

    private static instance: Utils | null;

    public static getInstance() {
        if (!this.instance) {
            this.instance = new this();
        }
        return this.instance;
    }

    public static releaseInstance() {
        if (this.instance) {
            this.instance = null;
        }
    }

    public getConfig<T>(setting: string) {
        return config.get<T>(setting);
    }

    private constructor() {
    }
}

export default Utils;