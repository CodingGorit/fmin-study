
enum ResultCodeEnum {
    ERROR = 50000,
    NETWORK_ERROR = 40002,
    DENIED = 40003,
    NOT_FOUND = 40004,
    SUCCESS = 20000,
    CREATE_SUCCESS = 20001,
    QUERY_SUCCESS = 20002,
    UPDATE_SUCCESS = 20003,
    DELETE_SUCCESS = 20004,
}

enum ResultMessageEnum {
    ERROR = "出错了，请联系管理员",
    NETWORK_ERROR = "网络出错了，等会再试试",
    DENIED = "你没有权限操作",
    NOT_FOUND = "糟糕，数据丢失了",
    SUCCESS = "操作成功",
    CREATE_SUCCESS = "创建成功",
    QUERY_SUCCESS = "查询成功",
    UPDATE_SUCCESS = "更新成功"
}

// 状态类型 只能是Code中所枚举的状态
// type codeType = keyof typeof ResultCodeEnum;
type msgType = keyof typeof ResultMessageEnum;

export {
    ResultCodeEnum,
    ResultMessageEnum,
    msgType
}