import { ResultCodeEnum, ResultMessageEnum } from "../enums/ResultEmums";

/**
 * @Author Gorit
 * @Date 2022年9月20日
 */
export class Result<T> {
    private code: ResultCodeEnum;
    private msg: ResultMessageEnum;
    private data: T | object | string;

    constructor(code: number, msg: ResultMessageEnum, data?: T | object) {
        this.code = code;
        this.msg = msg;
        this.data = data ?? "";
    }

    set setCode(code: number) {
        this.code = code;
    }

    set setData(data: T) {
        this.data = data;
    }

    set setMsg(msg: ResultMessageEnum) {
        this.msg = msg;
    }

    public static error (errMsg: ResultMessageEnum) {
        return new Result(ResultCodeEnum.ERROR, errMsg);
    }

    public toString() {
        return {
            code: this.code,
            msg: this.msg,
            data: this.data
        }
    }
}
