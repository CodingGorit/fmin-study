/**
 * @Author CodingGorit
 * @Date 2022年9月22日
 * @Abstract 参数校验
 */
import { Request, Response, NextFunction } from 'express';
const log = global.log;

function validateQuery (req: Request, res: Response, next: NextFunction, callback: () => void) {
    // 参数验证
    // if (!req.query.pagenum || req.query.pagenum <= 0) return res.send(null, 400, "pagenum 参数错误");
    // if (!req.query.pagesize || req.query.pagesize <= 0) return res.sendResult(null, 400, "pagesize 参数错误");
    log.info(`/user/info ${JSON.stringify(req.query)} ${JSON.stringify(req.body)}`);
    next();
};
