import { Express } from 'express';
import routes from '../routes';
import Utils from '../utils/Utils';
import registerExpressConfig from './registereeExpressConfig';
import initNacosClient from './registerNacosClient';

const log = global.log;
const utils = Utils.getInstance();
const PORT = utils.getConfig<number>("port");
const DEBUG = utils.getConfig<boolean>("DEBUG");

async function bootstrap(app: Express) {

    if (!DEBUG) {
        await initNacosClient();
    }

    // 加载相关配置信息
    registerExpressConfig(app);

    // 启动
    app.listen(PORT, async () => {
        log.info(`App is running at http://localhost:${PORT}`);
        routes(app);
    })
}

export default bootstrap;