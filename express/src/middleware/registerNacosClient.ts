/**
 * Created Date: Sunday, September 25th 2022, 12:01:19 am
 * Author: CodingGorit
 * -----
 * Last Modified: Sun Oct 16 2022
 * Modified By: CodingGorit
 * -----
 * Copyright (c) 2022 fmin-courses
 * ------------------------------------
 * Javascript will save your soul!
 */

'use strict';
import Utils from '../utils/Utils';
const log = global.log;
const { NacosNamingClient, NacosConfigClient } = require('nacos')

const utils = Utils.getInstance();

const TAG = "registerNacosClient:";
const SERVERNAME = utils.getConfig<string>("serviceName");
const PORT = utils.getConfig<number>("port");
const LOCALIP = utils.getConfig<string>("localIp");
const NACOSSERVER = utils.getConfig<string>("nacosServer");  // nacos服务端的地址
const NAMESPACE = utils.getConfig<string>("namespace"); //nacos命名空间

// 连接nacos
const client = new NacosNamingClient({
    logger: log,
    serverList: NACOSSERVER,
    namespace: NAMESPACE,
});

// 新建配置实例，接收推送
const config = new NacosConfigClient({
    serverAddr: NACOSSERVER,
    namespace: NAMESPACE
});


async function initNacosClient() {
    log.info(`${TAG} initNacosClientServiceDiscovery`);


    await registerServiceDiscovery(client);
}

/**
 * ServiceDiscovery
 */
async function registerServiceDiscovery(client: any) {
    await client.ready();

    // 开始注册
    log.info(`${TAG} register ${SERVERNAME} client SUCCESS`);
    await client.registerInstance(SERVERNAME, {
        ip: LOCALIP,
        port: PORT,
    });


    // subscribe instance
    // client.subscribe(SERVERNAME, hosts => {
    // log.warn(`${TAG} subscribe hosts => ${JSON.stringify(hosts)}`);
    // });


}

async function unregisterServiceDiscovery(client: any) {
    await client.deregisterInstance(SERVERNAME, {
        ip: LOCALIP,
        port: PORT,
    });
}

export default initNacosClient;
