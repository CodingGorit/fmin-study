/**
 * Created Date: Sunday, September 25th 2022, 10:00:35 pm
 * Author: CodingGorit
 * -----
 * Last Modified: Sun Oct 16 2022
 * Modified By: CodingGorit
 * -----
 * Copyright (c) 2022 fmin-courses
 * ------------------------------------
 * Javascript will save your soul!
 */

import { Express } from 'express';
import express from 'express';
import responseHeader from './responseHeader';
const log = global.log;

const TAG = "registerExpressConfig:";

function registerExpressConfig(app: Express) {
    log.info(`${TAG} init`);

    // For parsing application/json 
    app.use(express.json());

    // For parsing application/x-www-form-urlencoded 
    app.use(express.urlencoded({ extended: true }));

    // cors config
    app.all("*", responseHeader);
}


export default registerExpressConfig;