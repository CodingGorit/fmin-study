/**
 * @Author CodingGorit
 * @Date 2022年9月20日
 * @abstract 定义 type 和 interface
 */ 

// 通用结果集
export interface IResult<T> {
    code: number,
    msg: string,
    data?: T | string,
}