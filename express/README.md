## 介绍

> 一个简单的 express + ts 脚手架

### 支持

1. typescript 支持
1. nacos 支持
3. mysql 支持
4. config 支持
5. ...

### 配置

先去 config 修改 naocs 的配置，为你的公网 ip 等配置，不然你连接不了

```json
{
    "serviceName":"nacos's serviceName",
    "localIp":"127.0.0.1",
    "nacosServer":"xxx.com:8848",
    "namespace":"nacos's namespace",
}
```
## usage

devlopment

```typescript

// use npm
npm install

npm run dev

// use yarn
npm install -g yarn

yarn

yarn dev
```

production

```typescript
npm run prod

// or

yarn prod
```