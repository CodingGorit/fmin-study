## 补充

[游览器插件使用](https://plugin.csdn.net/?from=plugin_json_full )

```ts
"
Author: CodingGorit<gorit@qq.com>.     

Commit Date: 2022/10/9.      
Commit Type: UPDATE.   
Commit Description: UPDATE.   
"
```
## 今日问题总结

@小朱

1. 缩进问题（vs code 自带缩进）
2. 每行代码后面要加;（中英文分清楚）
3. sql语法
4. 通用接口字段 code: msg:"" data:{}
5. ~~mysql继续学习orm~~ 【先把 mysql crud基础的学熟练】
6. 把mysql语句写熟
7. 注释不要写太多
8. 可以写中间件，将代码再封装一次
9. 多个用put,单个用patch，三种传参方式混合使用
10. 目录有点乱
11. 下周熟悉框架代码，可以修改，不要提交

## 作业

1. 学习 ts 语法
2. 熟悉 express-ts 代码
3. 基于 express-ts 完成 上周的任务