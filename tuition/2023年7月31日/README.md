# File Header 配置

## 所有 Jetbrain 系列

配置方式

![Alt text](image.png)


配置内容

```shell
/**
 * @version v1.0
 * @Classname ${NAME}
 * @Description TODO
 * @Author CodingGorit
 * @Created on ${DATE} ${TIME}
 * @IDLE ${PRODUCT_NAME}
 * @Copyright (c) 2019 —— present fmin-courses TP Center. All rights reserved
**/
```

## vscode

安装插件

![Alt text](image-1.png)

配置

```json
{
        "psi-header.variables": [
        [
            "company",
            "fmin-courses TP Center"
        ],
        [
            "author",
            "CodingGorit" // 这里改成你自己的配置
        ],
        [
            "authoremail",
            "javafullstack2021@163.com" // 这里改成你自己的配置
        ],
        [
            "initials",
            "S.L"
        ],
    ],
    "psi-header.lang-config": [
        {
            "language": "lua",
            "begin": "--[[",
            "prefix": "--",
            "end": "--]]",
            "blankLinesAfter": 0
        },
        {
            "language": "python",
            "begin": "###",
            "prefix": "# ",
            "end": "###",
            "blankLinesAfter": 0,
            "beforeHeader": [
                "#!/usr/bin/env python",
                "# -*- coding:utf-8 -*-"
            ]
        },
        {
            "language": "javascript",
            "begin": "/**",
            "prefix": " * ",
            "end": " */",
            "blankLinesAfter": 2,
            "forceToTop": false
        },
        {
            "language": "typescript",
            "mapTo": "javascript"
        }
    ],
    "psi-header.templates": [
    
        {
            "language": "c",
            "template": [
                "File: <<filepath>>",
                "Project: <<projectpath>>",
                "Created Date: <<filecreated('YYYY-MM-DD HH:mm:ss')>>",
                "Author: <<author>>",
                "-----",
                "Last Modified: <<dateformat('YYYY-MM-DD HH:mm:ss')>>",
                "Modified By: ",
                "-----",
                "Copyright © 2019 —— <<year>> <<company>> All Rights Reserved",
                "",
                "<<licensetext>>",
                "-----",
                "HISTORY:",
                "Date      \tBy\tComments",
                "----------\t---\t----------------------------------------------------------"
            ],
            "changeLogCaption": "HISTORY:",
            "changeLogHeaderLineCount": 2,
            "changeLogEntryTemplate": [
                "<<dateformat('YYYY-MM-DD')>>\t<<initials>>\t"
            ]
        },
        {
            "language": "javascript",
            "template": [
                "Created Date: <<filecreated('dddd, MMMM Do YYYY, h:mm:ss a')>>",
                "Author: <<author>>",
                "-----",
                "Last Modified: ",
                "Modified By: ",
                "-----",
                "Copyright © 2019 —— <<year>> <<company>> All Rights Reserved",
                "------------------------------------",
                "Javascript will save your soul!"
            ]
        },
        {
            "language": "typescript",
            "mapTo": "javascript"
        }
    ],
}
```