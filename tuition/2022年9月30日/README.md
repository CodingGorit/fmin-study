## 问题点评

答疑
1. 异常拦截（）+ 全局拦截
2. FORM 表单 x2
3. header

## 异常拦截方案

```typescript

// get post delete put
common.all("/**", 
    function (req: Request, res: Response, next: NextFunction) {
    // 参数验证
    log.debug(`${TAG} 404 not found, request uri is => ${req.originalUrl}`);
    res.send(new Result(ResultCodeEnum.NOT_FOUND, ResultMessageEnum.NOT_FOUND).toString());
});
```

## FORM

```HTML

    <form action="http://localhost:1347/api/..." method="post" class="form-example">
        <div class="form-example">
          <label for="name">账号：</label>
          <input type="text" name="name" id="name" required>
        </div>
        <div class="form-example">
          <label for="password">密码：</label>
          <input type="password" name="password" id="password" required>
        </div>
        <div class="form-example">
          <input type="submit" value="Subscribe!">
        </div>
      </form>
```


## 合作开发接口

后端合作小任务（国庆完成）

单表 CRUD

数据创建 POST
数据修改（单条，多条）PATCH  PUT
数据删除（单条，多条）DELETE
数据查询 （单条、多条）GET、POST【分页】


## 网络请求方式（作业一）

练习如下网络请求方式（国庆完成）2天

1. WEB 端通用(FETCH: https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch)
2. axios (https://www.axios-http.cn/)
3. 微信小程序 wx.request

## V1.1 接口开发（作业二：待定）

蔡蔡提供 java 版本，小石、小朱 express 实现（待定）

1. mysql 数据 + express 交互
2. 分页数据
3. mock 数据【了解】


## 团队开发工作（TODO）

1. 版本管理
2. 。。。