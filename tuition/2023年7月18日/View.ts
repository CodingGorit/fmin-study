/**
 * Created Date: Tuesday, July 18th 2023, 12:13:05 am
 * Author: CodingGorit
 * -----
 * Last Modified: Sat Jul 22 2023
 * Modified By: CodingGorit
 * -----
 * Copyright © 2019 —— 2023 fmin-courses TP Center All Rights Reserved
 * ------------------------------------
 * Javascript will save your soul!
 */
type from = "View" | "CompositeView"

class View {

    constructor() {
        console.log("init in View's constructor");
        this.initialize("View");
    }

    static {
        console.log("init in View's static ");
    }

    /**
     * @public
     */
    initialize(type: from) {
        console.log(`init in ${type}"s initialize`);
    }
}

class CompositeView extends View {
    
    constructor(){
        super();
        console.log("init in CompositeView's constructor");
    }


    /**
     * @override
     */
    initialize() {
        console.log("init in CompositeView's initialize");
    }
}

const compositeView = new CompositeView();
compositeView.initialize();