/**
 * Created Date: Saturday, September 24th 2022, 8:27:51 pm
 * Author: CodingGorit
 * -----
 * Last Modified: Sat Sep 24 2022
 * Modified By: CodingGorit
 * -----
 * Copyright (c) 2022 fmin-courses
 * ------------------------------------
 * Javascript will save your soul!
 */

// https://expressjs.com/zh-cn/

// const let  var

const express = require('express');

const app = express();

// For parsing application/json 
app.use(express.json()); 
  
// For parsing application/x-www-form-urlencoded 
app.use(express.urlencoded({ extended:true })); 

// 当对主页发出 GET 请求时，响应“hello world”
app.get('/', function (req, res) {
    res.send('hello world');
});

// ================ 三种 传参方式 ==================

// http://localhost:1347/path/12
app.get("/path/:id", function(req, res) {
    res.status(200).send("recive path id is => " + req.params.id);
});

// http://localhost:1347/params?username=aaa&password=bbb
app.get("/params", function(req, res) {
    res.status(200).send("recive path id is => " + req.query.username + " pwd is =>" + req.query.password);
});

// 传对象
// http://localhost:1347/body
app.post("/body", function(req, res) {
    console.log(`qeury is => ${JSON.stringify(req.query)},params is => ${JSON.stringify(req.params)} ,body is => ${JSON.stringify(req.body)}`);
    res.status(200).send(JSON.stringify(req.body));
});

app.listen(1347, () => {
    console.log("App is listening localhost:1347!!!");
});

