# 2022年9月24日

1. ts 基础语法 及 常见应用场景
2. node-express
3. express-ts

## 一、开发环境搭建 及 Node 介绍

https://five-min.yuque.com/docs/share/d40b3b7e-a1dd-42ce-8c0c-0b68fa38f8ea?# 《Node.js 开发路线》


## 二、ts 基础 及 常见案例

> 前提 java 基础扎实，js 基础扎实

### 2.1 基础

https://www.tslang.cn/docs/handbook/basic-types.html


推荐

https://gitee.com/CodingGorit/fmin-study/blob/main/ts-study/study.md#gorit-%E7%9A%84-ts-%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B0

### 2.2 常见使用场景

一、函数限定传递的参数类型

```ts
function add(x: number, y: number): number {
    return x + y;
}

let myAdd = function(x: number, y: number): number { return x + y; };

```

二、面向对象写法

```ts
class Greeter {
    greeting: string;
    constructor(message: string) {
        this.greeting = message;
    }
    greet() {
        return "Hello, " + this.greeting;
    }
}

let greeter = new Greeter("world");

```

案例工具类编写

三、泛型

```ts

let arr1: string[] = ["1", "2"];
let arr : Array<string> = ["aaa", "b"];

// 也可以用在类里面

/**
 * @Author Gorit
 * @Date 2022年9月20日
 */
class Result<T> {
    private code: ResultCodeEnum;
    private msg: msgType;
    private data: T | object | string;

    constructor(code: number, msg: msgType, data?: T | object) {
        this.code = code;
        this.msg = msg;
        this.data = data ?? "";
    }

    set setCode(code: number) {
        this.code = code;
    }

    set setData(data: T) {
        this.data = data;
    }

    set setMsg(msg: msgType) {
        this.msg = msg;
    }

    public static error (errMsg: msgType) {
        return new Result(ResultCodeEnum.ERROR, errMsg);
    }

    public toString() {
        return {
            code: this.code,
            msg: this.msg,
            data: this.data
        }
    }
}

```

四、interface 高频

```ts

// java 的实体类
interface User {
    id: number;
    username: string;
    email?: string;
    password: string;
    password2?: string;
    role?: string;
}

// 可以用 类 来实现接口（java）
interface ClockInterface {
    currentTime: Date;
}

class Clock implements ClockInterface {
    currentTime: Date;
    constructor(h: number, m: number) { }
}

// http 网络请求库
interface Axios {
    defaults: AxiosRequestConfig;
    interceptors: {
        request: AxiosInterceptorManager<AxiosRequestConfig>,
        response: AxiosInterceptorManager<AxiosResponse>
    }

    request<T = any>(config: AxiosRequestConfig): AxiosPromise<T>;

    get<T = any>(url: string, config?: AxiosRequestConfig): AxiosPromise<T>;

    delete<T = any>(url: string, config?: AxiosRequestConfig): AxiosPromise<T>;

    head<T = any>(url: string, config?: AxiosRequestConfig): AxiosPromise<T>;

    options<T = any>(url: string, config?: AxiosRequestConfig): AxiosPromise<T>;

    post<T = any>(url: string, data?: any, config?: AxiosRequestConfig): AxiosPromise<T>;

    put<T = any>(url: string, data?: any, config?: AxiosRequestConfig): AxiosPromise<T>;

    patch<T = any>(url: string, data?: any, config?: AxiosRequestConfig): AxiosPromise<T>;

}



// 继承 ..
```

五、枚举

1. 默认枚举
2. 字符串枚举
3. 异常枚举

https://www.tslang.cn/docs/handbook/enums.html

```ts

```

六、type 类型别名


```ts

/**
 * Type 类型别名
 * 将内联类型抽离出来，从而实现类型可复用
 * 
 * type 别名 = 类型定义
 */

// 定义多种返回类型
type ResultType = number | string | object

// 定义 HTTP 请求方法
type Method = 'get' | 'GET'
    | 'delete' | 'DELETE'
    | 'head' | 'HEAD'
    | 'options' | 'OPTIONS'
    | 'post' | 'POST'
    | 'put' | 'PUT'
    | 'patch' | 'PATCH'

```

七、类型强转

ts 任何内容都可以看成对象，yots 来约束对象  

```ts
interface IResult<T> {
    code: number;
    msg: string;
    data: T
}

interface IPage: {
    currentPage: number, // 当前页
    pageSize: number, // 每页显示条数
    totalPage: number, // 总页数
    totalCount: number, // 总条数
    params: object, // 查询参数对象
    list: Array<any>, // 数据
    sortColumn: string, // 排序列
    sortMethod: string // 排序方式
},

const res = <IResult<IPage>>getUserList();

const res1 = getUserList() as IResult<IPage>

```


小作业：看看 express-ts 中用到了哪些类型

## 三、express

前后端交互示例

https://codinggorit.blog.csdn.net/article/details/106031911

作业：https://five-min.yuque.com/docs/share/8ad09708-3ba8-457e-aefe-42b886caed34?# 《V1.0 Java 接口文档》密码：hwgt