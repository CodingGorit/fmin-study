/**
 * Created Date: Saturday, September 24th 2022, 10:13:24 pm
 * Author: CodingGorit
 * -----
 * Last Modified: Sat Sep 24 2022
 * Modified By: CodingGorit
 * -----
 * Copyright (c) 2022 fmin-courses
 * ------------------------------------
 * Javascript will save your soul!
 */

class Utils {
    private static instance: Utils | null;

    public static getInstance() {
        if (!this.instance) {
            this.instance = new Utils();
        }
        return this.instance;
    }

    public static releaseInstance() {
        if (this.instance) {
            this.instance = null;
        }
    }

    public add(a: number, b: number) : number {
        return a + b;
    }

    private constructor () {

    }
}

const utils = Utils.getInstance();
utils.add(1,2);
