# git commit message 配置

1. 创建一个名为 `.gitmessage.txt` 的文件，填写如下内容，并放在一个全英文的目录，比如我的路径是 `D:\\globalConfig\.gitmessage.txt`

```shell
# 参考模板一
# Author:(昵称)<邮箱>

# Commit Date：<date>
# Commit Type: <type>
# Commit Description： <Body>

# <footer>

# type 类型字段包含:
#  init：初始化
#     feature：新功能（feature）
#     fix：修复bug
#     doc：文档（documentation）
#     style： 格式化 ESLint调整等（不影响代码运行的变动）
#     refactor：重构（即不是新增功能，也不是修改bug的代码变动）
#     test：增加测试
#  study：教学

# 影响范围：
#     用于说明 commit 影响的范围，比如修改的登录页、账户中心页等

# Body 部分是对本次 commit 的详细描述，可以分成多行

# Author: CodingGorit<gorit@qq.com>.     

# Commit Date: 2023/02/21.      
# Commit Type: code optimazation.  
# Commit Description: add MailMessageCodeEnum for MailManger.

# 参考模板二

feat: add useState callback  # 新需求开发
fix: fix js crash   # bug 模板
docs: update README.md
style: format code  # 格式化代码
refactor: restructure xxxx  # 重构
test: uat

```

2. 配置如下指令

```ts
// win 环境下要使用双斜杠

// 全局生效
git config --global commit.template D:\\globalConfig\\.gitmessage.txt

// 局部生效
git config --local commit.template D:\\globalConfig\\.gitmessage.txt
```

3. 尝试一下提交代码，git commit 不带 -m
